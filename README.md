# api

#### 介绍
	本文档的目的是为充值平台定义一个接口规范，以帮助商户技术人员快速接入充值平台充值接口，并快速掌握充值平台相关功能，便于尽快投入使用。
	商户：通过充值平台进行接口充值的商家。
	编码格式：统一为GBK；
	接口接入前需先进行白名单配置；   
	特别注意以下问题（否则出现相关问题导致代理商损失我方概不负责）：
 		1、代理商需完成充值请求、充值结果通知、充值订单查询三个接口编写，并与我们技术人员联调测试通过才可开放产品上线充值。	  
 		2、代理商在未得到我方系统明确返回的订单充值失败情况下，请不要将订单当失败处理，订单充值最终结果请等待系统回调或代理商下单时间超10分钟后再主动进行查询得到最终结果 。   

#### 使用说明
	源码直接copy到工程,然后将pom.xml中的依赖包引入项目,参考例子使用的 mer_id=hongxuda key=0b041b137776b157dca7e1ecd937be96

#### 订单发送接口 参考
    
```
SendOrder sendOrder = new SendOrder();
    	//商户号
    	sendOrder.setMer_id(mer_id);
    	//商户订单号,保证商户唯一 
    	sendOrder.setMer_oid("111111");
    	//充值手机号
    	sendOrder.setPhone_no("111111");
    	//整数,元为单位
    	sendOrder.setRecharge_amount("30");
    	//通知地址
    	//请求时间
    	sendOrder.setRequest_time(DateFormatUtils.format(new Date(), "yyyyMMddHHmmss"));
    	//业务类型 不可空 （0：话费；1：流量；2 :油卡类；3:视频互娱类;4：卡密提卡）
    	sendOrder.setRecharge_type("0");
        //订单发送地址
    	sendOrder.setUrl(sendUrl);
        //商户密钥
    	sendOrder.setKey(key);
        //cp_type 运营商 可空 移动：YD 电信：DX联通：LT 移动虚商：XSYD 电信虚商：XSDX 联通虚商：XSLT
        // 如该字段为空，以我方号段判断依据为准
    	sendOrder.setCp_type("");
        //fm 充值总面值 不可空 充值总面值=商品面值*商品数量
    	sendOrder.setFm("30");
        //nb 商品数量 不可空 您所需要充值的充值数量，暂时只支持数值1
    	sendOrder.setNb("1");
        //phone_no 充值号码 不可空 您所需要充值的帐号信息
    	sendOrder.setProduct_no("");
        //use_type 使用类型 不可空 业务类型为流量，使用类型 (0：漫游；1：本地；2：限时)，其他业务类型时使用类型默认为0；
    	sendOrder.setUse_type("0");
        //活动ID  该字段为移动（和包）资源使用，具体的值请联系运营人员配置
        sendOrder.setActivity_id("");
        //notify_url 通知地址 不可空 通知地址,根据协议2.3充值结果通知，返回充值结果
    	sendOrder.setNotify_url(noticeUrl);
    	
    	try {
		Map<String,Object> map =App.sendOrder(sendOrder);		
		//错误消息
		String msg = (String)map.get("msg");
		//参考错误码和结果
		String code = (String)map.get("code");
		String result = (String)map.get("result");
		//下单成功获取到泓旭达平台订单
		String supply_id = (String)map.get("data.supply_id");

		System.out.println(map);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
```

#### 订单发送接口 提交请求
	提交地址：	http://8.134.12.246:8090/recieverOrder 生产 http://8.134.10.153:8090/recieverOrder 测试
	请求方式	GET
| 参数名称 | 参数含义 | 可否为空 | 参数说明 |
|------|------|------|------|
|mer_id|	商家编号|	不可空|	商户注册的时候产生的一个商户ID|
|mer_oid|	商家订单编号|	不可空|	商户系统自己生成的订单号，限制32位以内。|
|product_no|	产品编号|	可为空|	Q币直充、油卡、视频互娱类、批量提卡类 产品编号必填。话费、流量业务为空；|
|product_face|	产品面值|	不可空|	您所充值的产品面值，流量单位为M，其它为元|
|product_num|	产品数量|	不可空|	您所需要充值的充值数量，只支持数值1|
|recharge_amount|扣款金额|	可空|	扣款金额=产品面额*销售折扣（用于检测双方折扣是否一致）为空则不校验|
|phone_no|	充值号码|	不可空|	您所需要充值的帐号信息|
|notify_url|	通知地址|	不可空|	通知地址,查看通知接口返回充值结果参数需要进行 传参数时encode处理 URLEncoder.encode(paramsMap.get(param), "gbk"),参与签名时不需要encode|
|request_time|	时间戳|	不可空|	请求时间戳，格式yyyyMMddHHmmss |
|recharge_type|	业务类型|	不可空|	待充值产品的业务类型：（0：话费；1：流量；2 :油卡类；3:视频互娱类；4:卡密提卡）|
|use_type|	使用类型|	不可空|	业务类型为流量，使用类型(0：漫游；1：本地；2：限时)，其他业务类型时使用类型默认为0；|
|activity_id|	活动ID|	可空|	该字段为移动（和包）资源使用，具体的值请联系运营人员配置|
|cp_type|	运营商|	可空|	移动：YD 电信：DX联通：LT移动虚商：XSYD 电信虚商：XSDX联通虚商：XSLT如该字段为空，以我方号段判断依据为准|
|sign|	签名|	不可空|	MD5(mer_id+mer_oid+product_face+product_num+recharge_amount+phone_no+notify_url+request_time+recharge_type+key) ，不区分大写|


	实例：
	http://8.134.10.153:8090/recieverOrder?phone_no=18774925797&recharge_amount=30&mer_id=hongxuda&recharge_type=0&sign=7581339756db3ea67085791c7b1ebfac&product_num=1&notify_url=http%3A%2F%2F9db09f473a3a.ngrok.io%2Ftest&product_no=&mer_oid=1606628141779&use_type=0&request_time=20201129133541&cp_type=&product_face=30


#### 订单发送接口 返回
| 参数名称 | 参数含义 | 可否为空 | 参数说明 |
|------|------|------|------|
|supply_id|返回泓旭达系统订单号|不可空|泓旭达系统生成的订单号|
			

- 返回XML举例，提交失败举例
	

 
```
<?xml version="1.0" encoding="gbk"?>
 	<response><result>false</result><code>256</code><msg>产品未开通或关闭</msg></response>
```


- 返回XML举例，提交成功举例
	


```
<?xml version="1.0" encoding="gbk"?>
<response><result>true</result><code>100</code><msg>操作成功</msg><data><supply_id>100013684762</supply_id></data></response>

```
	
 返回结果码
| result参数  |  Code参数 |  msg参数 |
|---|---|---|
|true	|100	|提交成功|
|false	|201	|必须参数为空|
|false	|202	|请求参数不符合规则（如订单号超过32位，请求类型非数字等)|
|false	|251	|签名错误|
|false	|252	|订单已存在|
|false	|253	|商家不存在或未启用|
|false	|254	|订单金额错误|
|false	|256	|渠道产品未开通|
|false	|261	|手机号鉴权失败|
|false	|274	|参数 tsp 超过10分钟，下单失败|
|false	|888	|商户余额不足|
|	|999	|未知异常，请进一步核实|
       
	
注意事项 
**说明: 请严格按上表result和code进行下单结果判断，result=true,同时code=100时，才是下单成功，上表的其他result、code组合为下单失败；**   
**没有明确得到上表罗列的result、code返回（包括未收到响应报文），请做存疑订单处理（主动查询或使用相同订单号进行重新下单，其中主动查询请参考订单查询接口说明）。** 
####  查询账户余额接口 参考
		
```
QueryAccountOrder  query = new QueryAccountOrder();
		query.setMer_id(mer_id);
		query.setKey(key);
		query.setUrl(queryAccountUrl);
		//300975381539，300975382024，300975382553
		query.setRequest_time(DateFormatUtils.format(new Date(), "yyyyMMddHHmmss"));
		try {
			Map<String,Object> map = App.queryAccountOrder(query);
			


			System.out.println(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
```
####  查询账户余额接口 提交
        提交地址	http://8.134.12.246:8090/queryAccount 生产 http://8.134.10.153:8090/queryAccount 测试
|参数名称   |参数含义   |可否为空   | 参数说明  |
|---|---|---|---|
|mer_id	|商户ID	|不可空	|商户ID|
|request_time	|时间戳	|不可空	|请求时间戳，格式yyyyMMddHHmmss|
|sign	|签名	|不可空	|MD5 (mer_id+request_time+key)不区分大写|
    实例:
http://127.0.0.1:38455/queryAccount?request_time=20201229131756&mer_id=test&sign=24ed27da7e5b8c0316fe12af24e4b328
####  查询账户余额接口 返回参数
|参数   |说明  |
|---|---|
|code			|返回码|
|	msg				|返回描述|
|	result			|查询结果|
|money|金额|
    返回XML举例,提交成功举例
```
<?xml version="1.0" encoding="gbk"?>
<response><result>true</result><code>100</code><money>7394.0500</money><msg>查询成功</msg></response>
```
    返回XML举例,提交失败举例
```
<?xml version="1.0" encoding="gbk"?>
<response><result>false</result><code>999</code><msg>系统异常</msg></response>
```  
**说明：账户金额判定方法：** 
**如果code=100时,根据money的值确定账户余额；因为是我司为先预扣,失败后退,金额会有变动.**
####  查询订单接口 参考
		
```
QueryOrder  query = new QueryOrder();
		query.setMer_id(mer_id);
		query.setKey(key);
		query.setUrl(queryUrl);
		query.setMer_oid("fake202011261754021998897354");
		query.setTsp(DateFormatUtils.format(new Date(), "yyyyMMddHHmmss"));
		try {
			Map<String,Object> map = App.queryOrder(query);
			String msg = (String)map.get("msg");
			String cp_id = (String)map.get("data.cp_id");
			String code = (String)map.get("code");
			String phone_no = (String)map.get("data.phone_no");
			String mer_id = (String)map.get("data.mer_id");
			String sale_discount = (String)map.get("data.sale_discount");
			String mer_oid = (String)map.get("data.mer_oid");
			String recharge_amount = (String)map.get("data.recharge_amount");
			String supply_id = (String)map.get("data.supply_id");
			String request_time = (String)map.get("data.request_time");
			String result = (String)map.get("result");
			String sale_value = (String)map.get("data.sale_value");
			String order_status = (String)map.get("data.order_status");
			String product_no = (String)map.get("data.product_no");
			String cp_ctype = (String)map.get("data.cp_ctype");


			System.out.println(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
```
####  查询订单接口 提交
	提交地址	http://8.134.12.246:8090/queryOrder 生产 http://8.134.10.153:8090/queryOrder 测试
|参数名称   |参数含义   |可否为空   | 参数说明  |
|---|---|---|---|
|mer_oid	|商户订单ID	|不可空	|商户自己生成的平台ID|
|mer_id	|商户ID	|不可空	|商户ID|
|tsp	|时间戳	|不可空	|请求时间戳，格式yyyyMMddHHmmss|
|sign	|签名	|不可空	|MD5 (mer_oid+mer_id+tsp+key)不区分大写|
	
	实例：
http://8.134.10.153:8090/queryOrder?tsp=20201129133759&mer_oid=1606413800166&mer_id=hongxuda&sign=a603af2fe3952771c1c67a90a5cdd842
####  查询订单接口 返回参数
|参数   |说明  |
|---|---|
|supply_id	|充值平台系统订单号|
|order_status	|订单状态:0 充值成功 1 or 2 充值中 3 充值失败| 
|mer_id	|商户ID|
|product_no	|商品ID|
|mer_oid	|商户订单号|
|phone_no	|充值账号|
|product_face	|产品面值|
|sale_discount	|扣款折扣|
|sale_value	|扣款金额|
|cp_id	|凭证流水|
|cp_ctype	|凭证类型（附录表）|
|request_time	|充值时间|
|code			|返回码|
|	msg				|返回描述|
|	result			|查询结果|	
	
	返回XML举例，提交失败举例	
	
```
<?xml version="1.0" encoding="gbk"?>
	<response><result>false</result><code>258</code><msg>订单不存在</msg></response>

```

	返回XML举例，提交充值中举例	
	
```
<?xml version="1.0" encoding="gbk"?>
<response><result>true</result><code>101</code><msg>查单成功，充值中</msg><data><supply_id>100013684743</supply_id><order_status>2</order_status><mer_id>hongxuda</mer_id><product_no>HFYDHUN****00003000</product_no><mer_oid>fake202011261754021998897354</mer_oid><phone_no>18774925797</phone_no><product_face>30</product_face><cp_id></cp_id><sale_discount>1</sale_discount><cp_ctype></cp_ctype><request_time>2020-11-26 17:54:03.0</request_time><sale_value>30</sale_value></data></response>



```


	返回XML举例，提交成功举例

```
<?xml version="1.0" encoding="gbk"?>
<response><result>true</result><code>100</code><msg>查单成功，充值成功</msg><data><supply_id>100013684751</supply_id><order_status>0</order_status><mer_id>hongxuda</mer_id><product_no>HFYDHUN****00003000</product_no><mer_oid>1606413800166</mer_oid><phone_no>18774925797</phone_no><product_face>30</product_face><cp_id>003420201127020422855098</cp_id><sale_discount>1</sale_discount><cp_ctype>1</cp_ctype><request_time>2020-11-27 02:04:07.0</request_time><sale_value>30</sale_value></data></response>


```

**说明：判断订单状态方法：** 

**1: 如果code=100  时,根据order_status的值确定订单状态；**

**2: 如果code=258：下单10分钟后查询到code=258订单不存在错误码则订单置为失败,10分钟之内不能处理。**

**3: 如果code=其他状态码：进一步查询或人工核实：**
		
```
//testmsg获取的通知请求体,注意使用gbk编码
		String testmsg = new String("{\"sale_value\":\"30\",\"supply_id\":\"100013684742\",\"order_status\":\"0\",\"mer_oid\":\"fake202011261210191960083057\",\"phone_no\":\"18774925797\",\"product_face\":\"30\",\"mer_id\":\"hongxuda\",\"sale_discount\":\"1\",\"sign\":\"AF443ECD6C73CBA06A2791312F2C24CE\",\"cp_id\":\"003420201126121040434401\",\"product_no\":\"HFYDHUN****00003000\",\"cp_ctype\":\"1\"}");
		System.out.println(testmsg);
		try {
			JSONObject json = App.signOrder(testmsg, key);
			String code = json.getString("code");
			if("1".equals(code)){
				//返回系统success 开启线程处理后续业务
				NoticeOrder order = (NoticeOrder)json.get("notice");
			}
			System.out.println(json.getString("code"));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
```
####  通知订单接口 提交
    通知地址：商户下单时提交的通知地址http post json格式
|参数名称   |参数含义   |可否为空   |参数说明   |
|---|---|---|---|			
|supply_id	|泓旭达系统订单号	|不可空	|泓旭达系统生成的订单号|
|order_status	|订单状态	|不可空	|订单状态只有两种，0为成功，3为失败|
|mer_id	|商户ID	|不可空	|商户在我们系统注册的ID|
|product_no	|商品编号	|不可空	|商品编号|
|mer_oid	|商户订单号	|不可空	|商户系统传递的订单ID|
|phone_no	|充值帐号	|不可空	|商户提交的充值帐号|
|product_face	|充值面额	|不可空	|提交给用户充值的面额|
|sale_value	|扣款金额	|不可空	|给用户充值成功的扣款金额|
|sale_discount	|销售折扣	|不可空	|实际扣款折扣|
|cp_id	|凭证流水	|可为空	|凭证流水和凭证号同时存在|
|cp_ctype	|凭证类型	|可为空	|0：其它；1：手支；2：网厅；3：统付；4：空充；5：卡密充，卡密的卡号；6：卡密；7：工号；8：boss；9：沃支付；10：计费；11：电网；12：省网；13：流量池子；14：全国电信；15：全国联通；|
|sign	|MD5签名	|不可空	|MD5(supply_id+order_status+mer_id+product_no+mer_oid+phone_no+product_face+key)MD5加密后字符串再大写|

通知的URL举例
{"sale_value":"30","supply_id":"100013684742","order_status":"0","mer_oid":"fake202011261210191960083057","phone_no":"18774925797","mer_id":"hongxuda","sale_discount":"1","sign":"AF443ECD6C73CBA06A2791312F2C24CE","cp_id":"003420201126121040434401","product_no":"HFYDHUN****00003000","product_face":"30","cp_ctype":"1"}

####  通知订单接口 说明
        实例:{"sale_value":"30","supply_id":"100013684742","order_status":"0","mer_oid":"fake202011261210191960083057","phone_no":"18774925797","mer_id":"hongxuda","sale_discount":"1","sign":"AF443ECD6C73CBA06A2791312F2C24CE","cp_id":"003420201126121040434401","product_no":"HFYDHUN****00003000","product_face":"30","cp_ctype":"1"}

	通知地址由泓旭达平台提供,商户验证签名后请立即开启业务处理线程异步处理业务并返回泓旭达平台success标识.未响应或者传其它标识系统将自动重发，超过3次不再发送


### 流量use_type 说明
|user_type   |参数含义   
|---|---|
|0	|漫游	|
|1	|本地
|2	|限时1天	|
|3	|慢充漫游|
|6	|限时2天	|
|7	|限时3天	|
|8	|限时5天	|
|9	|限时7天	|
|10	|限时10天|
|11	|限时15天|
|12	|限时20天|	

### 工具类
####  卡密解密 说明
   **前置条件**  
      
- ① 商户在平台已注册成功，得到了商户key；
- ② 对key进行MD5的32位小写加密;
- ③ MD5后的加密串前16位作为vi，后16位作为Aes加密密钥;

```JAVA
/*例：vi：6b4edd2ea18ca6ab ；  key: 4d2a873653ae136e；*/
 public class AesUtils
   {
     public static String encrypt(String input, String key, String vi)throws Exception
  {
        try
        {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.getBytes(), "AES"),
                new IvParameterSpec(vi.getBytes()));
               byte[] encrypted = cipher.doFinal(input.getBytes("utf-8"));
               return DatatypeConverter.printBase64Binary(encrypted);//BASE64做转码
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static String decrypt(String input, String key, String vi)
        throws Exception
    {
        try
        {
            byte[] inputByte = DatatypeConverter.parseBase64Binary(input);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getBytes(), "AES"),
                new IvParameterSpec(vi.getBytes()));
               byte[] encrypted = cipher.doFinal(inputByte);
               return new String(encrypted);//BASE64做转码
        }
        catch (Exception ex)
        {
            return null;
        }
    }
}
public static void main(String[] args)
        throws Exception
    {
     
     String dd = "5NUZrBYv+L/2CAvUwjlzw097oBnaaWRyRC0XzI8z6ygCd52TKRoJ0qS7mYdyW2EgKzxLMy8AZcv+yRVBmiUsh9fJSSbyGwVmsgkJiWcRkO6BeoUn5LHw9bJU55TV7jU10NJfI9TLnk5vDlXMCikoArTX/c6ghp+O5joF5XdB33vuiMkrr5FOQLutSvppRt3hQd4Ksj9Gd5oo/LrKQAZpYV3sMLjLww8vKr+amQ6NQaA=";
     String dataStr2= AesUtils.decrypt(dd, "6b4edd2ea18ca6ab","4d2a873653ae136e");
     System.out.println(dataStr2);
     String hex ="5B7B2265787069726554696D65223A22323032322D30342D3239222C22636172646E6F223A223131313131313131313131313135222C2263617264707764223A223131313131313131313131313135227D5D";
     byte[] s = DatatypeConverter.parseHexBinary(hex);  //import javax.xml.bind.DatatypeConverter;
     System.out.println(new String(s));
  } 

