package hongxuda.api.entity;
/**
泓旭达平台 0.0.1

*/
public class SendOrder {
	/** mer_id 商家编号 不可空 商户注册的时候产生的一个商户ID */
	public String mer_id;
	/** mer_oid 商家订单编号 不可空 商户系统自己生成的订单号，限制32位以内。 */
	public String mer_oid;
	/**
	 * product_no 产品编号 可为空 Q币直充、油卡、视频互娱类产品编号必填。 话费、流量业务为空；
	 */
	public String product_no;
	/**
	 * recharge_amount 产品金额 不可空 扣款金额=产品面额*销售折扣（用于检测双方折扣是否一致）
	 */
	public String recharge_amount;
	/**
	 * product_num 商品数量 不可空 您所需要充值的充值数量，暂时只支持数值1
	 */
	public String product_num;
	/**
	 * product_face 不可空 您所充值的产品面值，流量单位为M，其它为元
	 */
	public String product_face;
	/**
	 * phone_no 充值号码 不可空 您所需要充值的帐号信息
	 */
	public String phone_no;
	/**
	 * notify_url 通知地址 不可空 通知地址,根据协议2.3充值结果通知，返回充值结果
	 */
	public String notify_url;
	/**
	 * request_time 时间戳 不可空 请求时间戳，格式yyyyMMddHHmmss 接口方订单接收时间 - 600秒<request_time
	 * 
	 */
	public String request_time;
	/**
	 * recharge_type 业务类型 不可空 待充值产品的业务类型：（0：话费；1：流量；2 :油卡类；3:视频互娱类）
	 */
	public String recharge_type;
	/**
	 * use_type 使用类型 不可空 业务类型为流量，使用类型 (0：漫游；1：本地；2：限时)，其他业务类型时使用类型默认为0；
	 * 
	 */
	public String use_type;
	/**
	 * cp_type 运营商 可空 移动：YD 电信：DX联通：LT 移动虚商：XSYD 电信虚商：XSDX 联通虚商：XSLT
	 * 如该字段为空，以我方号段判断依据为准
	 */
	public String cp_type;
	/**
	 * sign 签名 不可空
	 * MD5(mer_id+mer_oid+recharge_amount+phone_no+notify_url+request_time+recharge_type+key)
	 * ，不区分大写
	 */
	public String sign;

	public String url;
	public String key;

	public String getProduct_num() {
		return product_num;
	}
	/**
	 * product_num 商品数量 不可空 您所需要充值的充值数量，暂时只支持数值1
	 */
	public void setProduct_num(String product_num) {
		this.product_num = product_num;
	}

	public String getProduct_face() {
		return product_face;
	}
	/**
	 * product_face 不可空 您所充值的产品面值，流量单位为M，其它为元
	 */
	public void setProduct_face(String product_face) {
		this.product_face = product_face;
	}

	public String getKey() {
		return key;
	}

	/** 商户密钥 */
	public void setKey(String key) {
		this.key = key;
	}

	public String getUrl() {
		return url;
	}

	/**
	 * 提供的下单地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	public String getMer_id() {
		return mer_id;
	}

	/** mer_id 商家编号 不可空 商户注册的时候产生的一个商户ID */
	public void setMer_id(String mer_id) {
		this.mer_id = mer_id;
	}

	public String getMer_oid() {
		return mer_oid;
	}

	/** mer_oid 商家订单编号 不可空 商户系统自己生成的订单号，限制32位以内。 */
	public void setMer_oid(String mer_oid) {
		this.mer_oid = mer_oid;
	}

	public String getProduct_no() {
		return product_no;
	}

	/**
	 * product_no 产品编号 可为空 Q币直充、油卡、视频互娱类产品编号必填。 话费、流量业务为空；
	 */
	public void setProduct_no(String product_no) {
		this.product_no = product_no;
	}

	public String getRecharge_amount() {
		return recharge_amount;
	}

	/**
	 * recharge_amount 产品金额 不可空 扣款金额=产品面额*销售折扣（用于检测双方折扣是否一致）
	 */
	public void setRecharge_amount(String recharge_amount) {
		this.recharge_amount = recharge_amount;
	}

	

	public String getPhone_no() {
		return phone_no;
	}

	/**
	 * phone_no 充值号码 不可空 您所需要充值的帐号信息
	 */
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getNotify_url() {
		return notify_url;
	}

	/**
	 * notify_url 通知地址 不可空 通知地址,根据协议2.3充值结果通知，返回充值结果
	 */
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getRequest_time() {
		return request_time;
	}

	/**
	 * request_time 时间戳 不可空 请求时间戳，格式yyyyMMddHHmmss 接口方订单接收时间 - 600秒<request_time
	 * 
	 */
	public void setRequest_time(String request_time) {
		this.request_time = request_time;
	}

	public String getRecharge_type() {
		return recharge_type;
	}

	/**
	 * recharge_type 业务类型 不可空 待充值产品的业务类型：（0：话费；1：流量；2 :油卡类；3:视频互娱类）
	 */
	public void setRecharge_type(String recharge_type) {
		this.recharge_type = recharge_type;
	}

	public String getUse_type() {
		return use_type;
	}

	/**
	 * use_type 使用类型 不可空 业务类型为流量，使用类型 (0：漫游；1：本地；2：限时)，其他业务类型时使用类型默认为0；
	 * 
	 */
	public void setUse_type(String use_type) {
		this.use_type = use_type;
	}

	public String getCp_type() {
		return cp_type;
	}

	/**
	 * cp_type 运营商 可空 移动：YD 电信：DX联通：LT 移动虚商：XSYD 电信虚商：XSDX 联通虚商：XSLT
	 * 如该字段为空，以我方号段判断依据为准
	 */
	public void setCp_type(String cp_type) {
		this.cp_type = cp_type;
	}

	public String getSign() {
		return sign;
	}

	/**
	 * sign 签名 不可空
	 * MD5(mer_id+mer_oid+recharge_amount+phone_no+notify_url+request_time+recharge_type+key)
	 * ，不区分大写
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

}
