package hongxuda.api.entity;
/**
泓旭达平台 0.0.1

*/
public class QueryOrder {
	/** 商户自己生成的平台ID */
	public String mer_oid;

	/** 商户提供的订单查询地址 */
	public String url;

	/** 商户提供的订单查询地址 */
	public void setUrl(String url) {
		this.url = url;
	}

	/** 商户ID */
	public String mer_id;
	/** 请求时间戳，格式yyyyMMddHHmmss */
	public String tsp;
	public String key;

	public String getKey() {
		return key;
	}

	/** 商户密钥 */
	public void setKey(String key) {
		this.key = key;
	}

	public String getMer_oid() {
		return mer_oid;
	}

	/** mer_oid 商家订单编号 不可空 商户系统自己生成的订单号，限制32位以内。 */
	public void setMer_oid(String mer_oid) {
		this.mer_oid = mer_oid;
	}

	public String getMer_id() {
		return mer_id;
	}

	/** mer_id 商家编号 不可空 商户注册的时候产生的一个商户ID */
	public void setMer_id(String mer_id) {
		this.mer_id = mer_id;
	}

	public String getTsp() {
		return tsp;
	}

	/**
	 * tsp 时间戳 不可空 请求时间戳，格式yyyyMMddHHmmss 接口方订单接收时间 - 600秒<request_time
	 * 
	 */
	public void setTsp(String tsp) {
		this.tsp = tsp;
	}

	public String getUrl() {
		return url;
	}

}
