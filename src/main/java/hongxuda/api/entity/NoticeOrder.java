package hongxuda.api.entity;
/**
泓旭达平台 0.0.1

*/
public class NoticeOrder {

	/** 泓旭达系统生成的订单号 */
	public String supply_id;
	/** 订单状态只有两种，0为成功，3为失败 */
	public String order_status;
	/**
	 * 商户系统传递的订单ID
	 */
	public String mer_oid;
	/**
	 * 商户提交的充值帐号
	 */
	public String phone_no;
	/**
	 * 面额
	 */
	public String product_face;
	/** 商户在我们系统注册的ID */
	public String mer_id;
	/**
	 * 给用户充值成功的扣款金额
	 */
	public String sale_value;
	/**
	 * 实际扣款折扣
	 */
	public String sale_discount;
	/** 凭证流水 */
	public String cp_id;
	/**
	 * MD5(supply_id+order_status+mer_id+product_no+mer_oid+phone_no+recharge_amount+key)MD5加密后字符串再大写
	 */
	public String sign;
	/** 商品编号 */
	public String product_no;
	/**
	 * 0：其它；1：手支；2：网厅；3：统付；4：空充； 5：卡密充，卡密的卡号；6：卡密；7：工号；
	 * 8：boss；9：沃支付；10：计费；11：电网； 12：省网；13：流量池子；14：全国电信；15：全国联通；
	 */
	public String cp_ctype;

	public String getSale_value() {
		return sale_value;
	}

	/**
	 * 给用户充值成功的扣款金额
	 */
	public void setSale_value(String sale_value) {
		this.sale_value = sale_value;
	}

	public String getSupply_id() {
		return supply_id;
	}

	/** 泓旭达系统生成的订单号 */
	public void setSupply_id(String supply_id) {
		this.supply_id = supply_id;
	}

	public String getOrder_status() {
		return order_status;
	}

	/** 订单状态只有两种，0为成功，3为失败 */
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	public String getMer_oid() {
		return mer_oid;
	}

	/**
	 * 商户系统传递的订单ID
	 */
	public void setMer_oid(String mer_oid) {
		this.mer_oid = mer_oid;
	}

	public String getPhone_no() {
		return phone_no;
	}

	/**
	 * 商户提交的充值帐号
	 */
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	

	public String getProduct_face() {
		return product_face;
	}
/**
 * 面额
 * 
 * */
	public void setProduct_face(String product_face) {
		this.product_face = product_face;
	}

	public String getMer_id() {
		return mer_id;
	}

	/** 商户在我们系统注册的ID */
	public void setMer_id(String mer_id) {
		this.mer_id = mer_id;
	}

	public String getSale_discount() {
		return sale_discount;
	}

	/**
	 * 实际扣款折扣
	 */
	public void setSale_discount(String sale_discount) {
		this.sale_discount = sale_discount;
	}

	public String getCp_id() {
		return cp_id;
	}

	/** 凭证流水 */
	public void setCp_id(String cp_id) {
		this.cp_id = cp_id;
	}

	public String getSign() {
		return sign;
	}

	/**
	 * MD5(supply_id+order_status+mer_id+product_no+mer_oid+phone_no+recharge_amount+key)MD5加密后字符串再大写
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getProduct_no() {
		return product_no;
	}

	/** 商品编号 */
	public void setProduct_no(String product_no) {
		this.product_no = product_no;
	}

	public String getCp_ctype() {
		return cp_ctype;
	}

	/**
	 * 0：其它；1：手支；2：网厅；3：统付；4：空充； 5：卡密充，卡密的卡号；6：卡密；7：工号；
	 * 8：boss；9：沃支付；10：计费；11：电网； 12：省网；13：流量池子；14：全国电信；15：全国联通；
	 */
	public void setCp_ctype(String cp_ctype) {
		this.cp_ctype = cp_ctype;
	}

}
