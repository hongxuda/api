package hongxuda.api.entity;
/**
泓旭达平台 0.0.1

*/
public class QueryAccountOrder {
	/** 商户自己生成的平台ID */
	public String mer_id;
	/**
	 * request_time 时间戳 不可空 请求时间戳，格式yyyyMMddHHmmss 
	 * 
	 */
	public String request_time;
	/**
	 * sign 签名 不可空
	 * MD5(mer_id+request_time+key)
	 * ，不区分大写
	 */
	public String sign;
	/** 商户提供的账户余额查询地址 */
	public String url;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String key;
	public String getMer_id() {
		return mer_id;
	}
	public void setMer_id(String mer_id) {
		this.mer_id = mer_id;
	}
	public String getRequest_time() {
		return request_time;
	}
	public void setRequest_time(String request_time) {
		this.request_time = request_time;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
}
