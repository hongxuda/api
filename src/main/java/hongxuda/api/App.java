package hongxuda.api;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;

import hongxuda.api.entity.NoticeOrder;
import hongxuda.api.entity.QueryAccountOrder;
import hongxuda.api.entity.QueryOrder;
import hongxuda.api.entity.SendOrder;
import hongxuda.api.util.HttpClientUtil;
import hongxuda.api.util.StringUtil;
import hongxuda.api.util.TextXmlResolver;

/**
 * 泓旭达平台api java-sdk 0.0.1
 *
 */
public class App {
	private static Logger log = LoggerFactory.getLogger(App.class);

	public static Map<String, Object> queryAccountOrder(QueryAccountOrder queryOrder) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		StringBuffer signString = new StringBuffer();
		signString
				.append(StringUtil.replaceNullToBlank(queryOrder.getMer_id()))
				.append(StringUtil.replaceNullToBlank(queryOrder.getRequest_time()))
				.append(StringUtil.replaceNullToBlank(queryOrder.getKey()));
		String sign = DigestUtils.md5Hex(signString.toString().getBytes("gbk"));
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("mer_id", queryOrder.getMer_id());
		paramsMap.put("request_time", queryOrder.getRequest_time());
		paramsMap.put("sign", sign);

		try {

			String respones = HttpClientUtil.httpClientGet(queryOrder.getUrl(), paramsMap, "gbk");
			System.out.println(respones);
			log.info("queryOrder:返回参数:" + respones);
			TextXmlResolver text = new TextXmlResolver();
			map = text.decode(respones, "utf-8");
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;

	}
	public static Map<String, Object> queryOrder(QueryOrder queryOrder) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		StringBuffer signString = new StringBuffer();
		signString.append(StringUtil.replaceNullToBlank(queryOrder.getMer_oid()))
				.append(StringUtil.replaceNullToBlank(queryOrder.getMer_id()))
				.append(StringUtil.replaceNullToBlank(queryOrder.getTsp()))
				.append(StringUtil.replaceNullToBlank(queryOrder.getKey()));
		String sign = DigestUtils.md5Hex(signString.toString().getBytes("gbk"));
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("mer_id", queryOrder.getMer_id());
		paramsMap.put("mer_oid", queryOrder.getMer_oid());
		paramsMap.put("tsp", queryOrder.getTsp());
		paramsMap.put("sign", sign);

		try {

			String respones = HttpClientUtil.httpClientGet(queryOrder.getUrl(), paramsMap, "gbk");
			System.out.println(respones);
			log.info("queryOrder:返回参数:" + respones);
			TextXmlResolver text = new TextXmlResolver();
			map = text.decode(respones, "utf-8");
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;

	}

	public static Map<String, Object> sendOrder(SendOrder sendOrder) throws UnsupportedEncodingException {

		Map<String, Object> map = new HashMap<String, Object>();
		StringBuffer signString = new StringBuffer();
		signString.append(StringUtil.replaceNullToBlank(sendOrder.getMer_id()))
				.append(StringUtil.replaceNullToBlank(sendOrder.getMer_oid()))
				.append(StringUtil.replaceNullToBlank(sendOrder.getProduct_face()))
				.append(StringUtil.replaceNullToBlank(sendOrder.getProduct_num()))

				.append(StringUtil.replaceNullToBlank(sendOrder.getRecharge_amount()))
				.append(StringUtil.replaceNullToBlank(sendOrder.getPhone_no()))
				.append(StringUtil.replaceNullToBlank(sendOrder.getNotify_url()))
				.append(StringUtil.replaceNullToBlank(sendOrder.getRequest_time()))
				.append(StringUtil.replaceNullToBlank(sendOrder.getRecharge_type()))
				.append(StringUtil.replaceNullToBlank(sendOrder.getKey()));
		System.out.println(signString.toString());
		String sign = DigestUtils.md5Hex(signString.toString().getBytes("gbk"));
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("mer_id", sendOrder.getMer_id());
		paramsMap.put("mer_oid", sendOrder.getMer_oid());
		paramsMap.put("recharge_amount", sendOrder.getRecharge_amount());
		paramsMap.put("phone_no", sendOrder.getPhone_no());
		paramsMap.put("notify_url", sendOrder.getNotify_url());
		paramsMap.put("request_time", sendOrder.getRequest_time());
		paramsMap.put("use_type", sendOrder.getUse_type());
		paramsMap.put("cp_type", sendOrder.getCp_type());
		paramsMap.put("product_num", sendOrder.getProduct_num());
		paramsMap.put("product_face", sendOrder.getProduct_face());
		paramsMap.put("product_no", sendOrder.getProduct_no());

		paramsMap.put("recharge_type", sendOrder.getRecharge_type());
		paramsMap.put("sign", sign);
		//
		//
		try {

			String respones = HttpClientUtil.httpClientGet(sendOrder.getUrl(), paramsMap, "gbk");
			System.out.println(respones);
			log.info("sendOrder 返回参数:" + respones);
			TextXmlResolver text = new TextXmlResolver();
			map = text.decode(respones, "utf-8");
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * JSONObject 中 code 1 代表校验通过可以进行后续业务处理 获取notice进行业务处理 code 0 代表校验不通过
	 * 
	 * @throws Exception
	 */
	public static JSONObject signOrder(String message, String key) throws Exception {
		// {"sale_value":"30","supply_id":"100013684742","order_status":"0","mer_oid":"fake202011261210191960083057","phone_no":"18774925797","recharge_amount":"30","mer_id":"hongxuda","sale_discount":"1","sign":"AF443ECD6C73CBA06A2791312F2C24CE","cp_id":"003420201126121040434401","product_no":"HFYDHUN****00003000","cp_ctype":"1"}
		JSONObject json = JSONObject.parseObject(message);
		String supply_id = json.getString("supply_id");
		String phone_no = json.getString("phone_no");
		String product_face = json.getString("product_face");
		String mer_id = json.getString("mer_id");
		String sign = json.getString("sign");
		String cp_id = json.getString("cp_id");
		String product_no = json.getString("product_no");
		String order_status = json.getString("order_status");
		String mer_oid = json.getString("mer_oid");
		String sale_value = json.getString("sale_value");
		String sale_discount = json.getString("sale_discount");
		String cp_ctype = json.getString("cp_ctype");
		StringBuffer buf = new StringBuffer();
		// MD5(supply_id+order_status+mer_id+product_no+mer_oid+phone_no+recharge_amount+key)MD5加密后字符串再大写

		buf.append(StringUtil.replaceNullToBlank(supply_id))
		.append(StringUtil.replaceNullToBlank(order_status))
				.append(StringUtil.replaceNullToBlank(mer_id))
				.append(StringUtil.replaceNullToBlank(product_no))
				.append(StringUtil.replaceNullToBlank(mer_oid))
				.append(StringUtil.replaceNullToBlank(phone_no))
				.append(StringUtil.replaceNullToBlank(product_face))
				.append(StringUtil.replaceNullToBlank(key));
		if (sign.equals(DigestUtils.md5Hex(buf.toString().getBytes("gbk")).toUpperCase())) {
			json.put("code", "1");
			NoticeOrder notice = new NoticeOrder();
			notice.setCp_ctype(cp_ctype);
			notice.setCp_id(cp_id);
			notice.setMer_id(mer_id);
			notice.setMer_oid(mer_oid);
			notice.setOrder_status(order_status);
			notice.setPhone_no(phone_no);
			notice.setProduct_no(product_no);
			notice.setProduct_face(product_face);
			notice.setSale_discount(sale_discount);
			notice.setSale_value(sale_value);
			notice.setSign(sign);
			notice.setSupply_id(supply_id);
			json.put("notice", notice);
		} else {
			json.put("code", "0");

		}

		return json;
	}

}
